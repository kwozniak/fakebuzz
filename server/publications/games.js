Meteor.publish("games", function(params){
    if (params)
        return Games.find({_id:params._id});
    else return null;
});

Meteor.publish("gameTypes", function(){
    return GameTypes.find();
});

Meteor.publish("knowledgeBases", function(){
    return KnowledgeBases.find();
});

Meteor.publish("questions", function(){
    return Questions.find();
});

Meteor.publish("images", function(){
    return Images.find();
});