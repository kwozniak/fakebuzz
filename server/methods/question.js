
function updateObjectIsValid (obj) {
    if (_.has(obj,"base", "answers", "text", "_id", "image", "answerIsImage")) {
        if (_.isString(obj.base) && _.isString(obj.text) && _.isString(obj._id) && _.isArray(obj.answers) && obj.answers.length == 4) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

Meteor.methods({
    "addQuestionToKnowlegdeBase": function(params) {
        if (Meteor.user()) {
            if (KnowledgeBases.findOne({
                _id: params.base
            })) {
                params = _.pick(params, "base", "answers", "text", "image", "answerIsImage");
                return Questions.insert(params);
            } else {
                throw new Meteor.Error(403, "Base not exist");
            }
        } else {
            throw new Meteor.Error(400, "Not logged");
        }
    },
    "updateQuestionToKnowlegdeBase": function(params) {
        if (Meteor.user()) {
            var id = params._id;
            if (Questions.findOne({
                _id: params._id
            })) {
                params = _.pick(params, "base", "answers", "text", "image", "_id", "answerIsImage");
                if (updateObjectIsValid(params)) {
                    if (!params.image || params.image === null || params.image === "") {
                        params = _.omit(params, 'image');
                    } else {

                    }
                    params = _.omit(params, '_id');
                    return Questions.update({_id: id}, {$set: params});
                } else {
                    throw new Meteor.Error(401, "Invalid data");
                }
            } else {
                throw new Meteor.Error(404, "Base not exist");
            }
        } else {
            throw new Meteor.Error(400, "Not logged");
        }
    },
    "removeQuestion": function(params) {
        if (Meteor.user()) {
            var data = _.pick(params, '_id');
            if (Questions.findOne({
                _id: data._id
            })) {
                return Questions.remove(data._id);
            } else {
                throw new Meteor.Error(403, "Question not exist");
            }
        } else {
            throw new Meteor.Error(400, "Not logged");
        }
    },
});