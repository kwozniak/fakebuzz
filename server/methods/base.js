Meteor.methods({
    "addKnowledgeBase": function(params) {
        if (Meteor.user()) {
            var data = _.pick(params, 'name', 'mainCategory', 'price', 'description', 'image');
            if (!KnowledgeBases.findOne({
                name: data.name,
                mainCategory: data.mainCategory
            })) {
                return KnowledgeBases.insert(data);
            } else {
                throw new Meteor.Error(403, "Base exist");
            }
        } else {
            throw new Meteor.Error(400, "Not logged");
        }
    },
    "removeKnowledgeBase": function(params) {
        if (Meteor.user()) {
            var data = _.pick(params, '_id');
            if (KnowledgeBases.findOne({
                _id: data._id
            })) {
                return KnowledgeBases.remove(data._id);
            } else {
                throw new Meteor.Error(403, "Base not exist");
            }
        } else {
            throw new Meteor.Error(400, "Not logged");
        }
    },
    "updateKnowledgeBase": function(params) {
        if (Meteor.user()) {
            var _id = params._id;
            var data = _.pick(params, 'name', 'mainCategory', 'price', 'description', 'image');
            if (!KnowledgeBases.findOne({
                name: data.name,
                _id: {
                    $ne: _id
                }
            })) {
                if (!data.image || data.image === null || data.image === "") {
                    data = _.omit(data, 'image');
                } else {

                }
                return KnowledgeBases.update({
                    _id: _id
                }, {
                    $set: data
                });
            } else {
                throw new Meteor.Error(403, "Exists base with the same name");
            }
        } else {
            throw new Meteor.Error(400, "Not logged");
        }
    },
});