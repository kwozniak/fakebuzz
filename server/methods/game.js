var io = Meteor.require('socket.io').listen(80);

io.on('connection', function(socket) {
    console.log('a user connected');

    socket.on("joinToGame", function(data) {
        var gameCode = data.code;
        var roomName = "/" + gameCode;

        socket.join(roomName, function(err) {
            if (err) {
                console.log(err);
            } else {
                socket.isServer = data.isServer;
                socket.roomName = roomName;
                socket.playerName = data.name || "server";

                console.log('a ' + socket.playerName + ' joined to ' + socket.roomName);

                socket.emit("joinedToGame", data);
                io.to(roomName).emit("userJoined", {
                    name: data.name
                });
            }
        });
    });

    socket.on("sendAnswer", function(data) {
        var gameCode = data.code;
        var roomName = "/" + gameCode;
        io.to(roomName).emit("getAnswer", data);
    })

    socket.on('disconnect', function() {
        var playerName = socket.playerName;
        var roomName = socket.roomName;

        if (socket.isServer) {
            console.log("server disconnected");
            var room = io.to(roomName);
            if (room) {
                while (room.sockets.length != 0) {
                    if (_.isNumber(room.sockets.length)) {
                        room.sockets[room.sockets.length - 1].disconnect();
                    };
                }
            }
        } else {
            console.log(playerName, " disconnected");
            io.to(roomName).emit("userDisconnected", {
                name: playerName
            });
        }
    })
});



function reverse(s) {
    return s.split("").reverse().join("");
}

function generateCode() {
    // aby zamienić na inta nalezy dopełnić zerami do 26 znaków
    var uuid = Meteor.uuid().replace(/-/g, "");
    uuid = reverse(parseInt(uuid, 16).toString(32));
    return reverse(uuid.replace(/^0+/, ''));
}

Meteor.methods({
    "createNewGame": function(params) {
        var ret = {},
            game = {};
        game.ownerId = Meteor.userId || "";
        game.creationDate = new Date();
        game.code = generateCode();
        game.players = [];

        while (Games.findOne({
            code: game.code
        })) {
            game.code = generateCode();
        }

        gameCode = game.code;
        ret._id = Games.insert(game);
        ret.code = game.code;

        return ret;
    }
});

Meteor.methods({
    "joinToGame": function(params) {
        var game = {};
        var selector = {
            code: params.code
        };
        game = Games.findOne(selector);
        if (_.isUndefined(game)) {
            throw new Meteor.Error(404, "Cannot find game");
        }

        if (_.find(game.players, function(player) {
            return player.name === params.name && player.connected == true;
        })) {
            throw new Meteor.Error(405, "Player name exist");
        };

        if (_.find(game.players, function(player) {
            return player.name === params.name;
        })) {
            var selectorActualPlayer = _.extend(selector, {
                "players.name": params.name
            });
            Games.update(selectorActualPlayer, {
                $set: {
                    "players.$.connected": true
                }
            });
        } else {
            Games.update(selector, {
                $push: {
                    players: {
                        name: params.name,
                        avatarPath: "/clipart/" + game.players.length + ".png",
                        connected: true
                    }
                }
            });
        }


        return game;
    },
    "setPlayerAsDisconnected": function(params) {
        var game = {};
        var selector = {
            code: params.code,
            "players.name": params.name
        };
        game = Games.findOne(selector);
        if (_.isUndefined(game)) {
            throw new Meteor.Error(404, "Cannot find game");
        }

        if (_.find(game.players, function(player) {
            return player.name === params.name;
        })) {
            Games.update(selector, {
                $set: {
                    "players.$.connected": false
                }
            });
        };
        return game;
    }
});