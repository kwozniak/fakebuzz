function createGame(socket) {

    Meteor.call("createNewGame", {}, function(err, ret) {
        if (!err) {
            if (!window.userSocketIO) {
                window.userSocketIO = socket;
            }
            Session.set("gameData", ret);

            var post = {
                code: ret.code,
                isServer: true
            };
            socket.on("joinedToGame", function(data) {
                usersPoints.remove({});
                Router.go('serverGameLobby');
            });
            socket.emit("joinToGame", post, function(joinErr, joinRet) {
                console.log(joinErr, joinRet);
            });
        } else {
            alert(err);
        }
    });
}

Template.homePage.events({
    'click [name="new-game-btn"]': function(event) {
        if (!window.userSocketIO) {
            var socket = io(window.location.hostname + ":80");
            socket.on('connect', function() {
                createGame(socket);
            });
            socket.on('disconnect', function() {
                Session.set("gameData", null);
            });
        } else {
            createGame(window.userSocketIO);
        }
    },
    'click [name="play-game-btn"]': function(event) {
        Router.go('clientJoinToGame');
    }

});

Template.homePage.rendered = function () {
        var $frame = $('#effects');
        var $wrap  = $frame.parent();

        // Call Sly on frame
        $frame.sly({
            horizontal: 1,
            itemNav: 'forceCentered',
            smart: 1,
            activateMiddle: 1,
            activateOn: 'click',
            mouseDragging: 1,
            touchDragging: 1,
            releaseSwing: 1,
            startAt: 0,
            scrollBar: $wrap.find('.scrollbar'),
            scrollBy: 1,
            speed: 300,
            elasticBounds: 1,
            easing: 'swing',
            dragHandle: 1,
            dynamicHandle: 1,
            clickBar: 1,

            // Buttons
            prev: $wrap.find('.prev'),
            next: $wrap.find('.next')
        });
};