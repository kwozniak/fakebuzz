Template.configKnowledgeBases.rendered = function() {
    $('.basesModal').on('hidden.bs.modal', function() {
        Session.set("configActualSelectedKnowledgeBase", null);
        $(this).find("form").trigger('reset');
    });
};


Template.configKnowledgeBases.helpers({
    base: function() {
        return KnowledgeBases.find();
    },
    actualBase: function() {
        return KnowledgeBases.findOne({
            _id: Session.get("configActualSelectedKnowledgeBase")
        });
    },
    getImageUrl: function(id) {
        var image = Images.find(id).fetch()[0];
        if (image) {
            return image.url();
        }
    }
});


Template.configKnowledgeBases.events({
    "submit #configAddKnowledgeBaseForm": function(event) {
        event.preventDefault();
        var $form = $(event.target),
            $modal = $(event.target).closest(".modal"),
            $name = $form.find("#configAddBaseName"),
            name = $name.val(),
            $mainCategory = $form.find("#configAddBaseMainCategory"),
            mainCategory = $mainCategory.val(),
            $price = $form.find("#configAddBasePrice"),
            price = $price.val(),
            $description = $form.find("#configAddBaseDescription"),
            description = $description.val(),
            $image = $form.find("#configAddBaseImage");

        var file = $image[0].files[0];

        if (file) {
            Images.insert(file, function(err, fileObj) {
                if (!err) {
                    addBase(fileObj._id);
                };
            });
        } else {
            addBase("");
        }

        function addBase(image) {
            console.log(image);
            var post = {
                image: image,
                name: name,
                mainCategory: mainCategory,
                price: price,
                description: description
            };
            Meteor.call('addKnowledgeBase', post, function(error, result) {
                if (error) {
                    alert(error);
                } else {
                    $modal.modal('hide');
                }
            });
        }

    },
    "submit #configEditKnowledgeBaseForm": function(event) {
        event.preventDefault();
        var $form = $(event.target),
            $modal = $(event.target).closest(".modal"),
            $name = $form.find("#configEditBaseName"),
            name = $name.val(),
            $mainCategory = $form.find("#configEditBaseMainCategory"),
            mainCategory = $mainCategory.val(),
            $price = $form.find("#configEditBasePrice"),
            price = $price.val(),
            $description = $form.find("#configEditBaseDescription"),
            description = $description.val(),
            $image = $form.find("#configEditBaseImage");

        var file = $image[0].files[0];

        if (file) {
            Images.insert(file, function(err, fileObj) {
                if (!err) {
                    editBase(fileObj._id);
                };
            });
        } else {
            editBase("");
        }

        function editBase (image) {
            var post = {
                _id: Session.get("configActualSelectedKnowledgeBase"),
                name: name,
                mainCategory: mainCategory,
                price: price,
                image: image,
                description: description
            };
            Meteor.call('updateKnowledgeBase', post, function(error, result) {
                if (error) {
                    alert(error);
                } else {
                    $modal.modal('hide');
                }
            });
        }

    },
    "click [name='configKnowledgeBaseRemoveButton']": function(event) {
        event.preventDefault();
        var $btn = $(event.target),
            _id = $btn.attr("_id");

        var post = {
            _id: _id
        };
        Meteor.call('removeKnowledgeBase', post, function(error, result) {
            if (error) {
                alert(error);
            } else {

            }
        });
    },
    "click [name='configKnowledgeBaseEditButton']": function(event) {
        event.preventDefault();
        Session.set("configActualSelectedKnowledgeBase", $(event.target).attr("_id"));
        $('#configEditBase').modal({
            show: true
        });
    }
});