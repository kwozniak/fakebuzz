Template.configQuestions.rendered = function() {
    var baseSelectId = "#configQuestionsBasesSelect";
    $(baseSelectId).select2({
        allowClear: false,
        placeholder: "Wybierz bazę pytań",
        width: "240"
    });
    $(baseSelectId).on("select2-selecting", function(e) {
        var id = e.object.id;
        Session.set("configActualSelectedKnowledgeBase", id);
    });
    Deps.autorun(function() {
        $(baseSelectId).select2("val", Session.get("configActualSelectedKnowledgeBase"));
    });

    Deps.autorun(function() {
        var baseId = Session.get("configActualSelectedKnowledgeBase");
        Meteor.subscribe("questions", baseId);
    });

    $('.questionsModal').on('hidden.bs.modal', function() {
        Session.set("configActualSelectedQuestion", null);
        $(this).find("form").trigger('reset');
    });


};

Template.configQuestions.helpers({
    base: function() {
        return KnowledgeBases.find();
    },
    question: function() {
        return Questions.find({
            base: Session.get("configActualSelectedKnowledgeBase")
        });
    },
    actualQuestion: function() {
        return Questions.findOne({
            _id: Session.get("configActualSelectedQuestion")
        });
    },
    getImageUrl: function(id) {
        var image = Images.find(id).fetch()[0];
        if (image) {
            return image.url();
        }
    },
    answerTypeIsText: function() {
        return Session.equals("configQuestionsAnswersType","text") || !Session.get("configQuestionsAnswersType");
    }
});

Template.configQuestions.events({
    "click [name='configQuestionsEditButton']": function(event) {
        event.preventDefault();
        Session.set("configActualSelectedQuestion", $(event.target).attr("_id"));
        $('#configEditQuestions').modal({
            show: true
        });
    },
    "click [name='configQuestionsRemoveButton']": function(event) {
        event.preventDefault();
        var $btn = $(event.target),
            _id = $btn.attr("_id");

        var post = {
            _id: _id
        };
        Meteor.call('removeQuestion', post, function(error, result) {
            if (error) {
                alert(error);
            } else {

            }
        });
    },
    "submit #configAddQuestionsForm": function(event) {
        event.preventDefault();
        var $form = $(event.target),
            $modal = $(event.target).closest(".modal"),
            $text = $form.find("#configAddQuestionsText"),
            $goodA = $form.find("#configAddQuestionsGoodAnswer"),
            $badA1 = $form.find("#configAddQuestionsBadAnswer1"),
            $badA2 = $form.find("#configAddQuestionsBadAnswer2"),
            $badA3 = $form.find("#configAddQuestionsBadAnswer3"),
            text = $text.val(),
            goodA = $goodA.val(),
            badA1 = $badA1.val(),
            badA2 = $badA2.val(),
            badA3 = $badA3.val(),
            id = Session.get("configActualSelectedKnowledgeBase"),
            $image = $form.find("#configAddQuestionsImage"),
            $answerIsImage = $("input#configAddQuestionsOptionsAnswerType"),
            answerIsImage = $answerIsImage.prop('checked');
        if(answerIsImage){
            var goodAnswerImage = $goodA[0].files[0], 
                badAnswerImage1 = $badA1[0].files[0], 
                badAnswerImage2 = $badA2[0].files[0], 
                badAnswerImage3 = $badA3[0].files[0];

            goodA = Images.insert(goodAnswerImage)._id;
            badA1 = Images.insert(badAnswerImage1)._id;
            badA2 = Images.insert(badAnswerImage2)._id;
            badA3 = Images.insert(badAnswerImage3)._id;
        }

        var file = $image[0].files[0];

        if (file) {
            Images.insert(file, function(err, fileObj) {
                if (!err) {
                    addQuestion(fileObj._id);
                };
            });
        } else {
            addQuestion("");
        }

        function addQuestion(image) {
            var post = {
                base: id,
                text: text,
                image: image,
                answerIsImage: answerIsImage,
                answers: [
                    goodA, badA1, badA2, badA3
                ]
            };
            Meteor.call('addQuestionToKnowlegdeBase', post, function(error, result) {
                if (error) {
                    alert(error);
                } else {
                    $modal.modal('hide');
                }
            });
        }
    },
    "submit #configEditQuestionsForm": function(event) {
        event.preventDefault();
        var $form = $(event.target),
            $modal = $(event.target).closest(".modal"),
            $text = $form.find("#configEditQuestionsText"),
            $goodA = $form.find("#configEditQuestionsGoodAnswer"),
            $badA1 = $form.find("#configEditQuestionsBadAnswer1"),
            $badA2 = $form.find("#configEditQuestionsBadAnswer2"),
            $badA3 = $form.find("#configEditQuestionsBadAnswer3"),
            text = $text.val(),
            goodA = $goodA.val(),
            badA1 = $badA1.val(),
            badA2 = $badA2.val(),
            badA3 = $badA3.val(),
            baseId = Session.get("configActualSelectedKnowledgeBase"),
            qId = Session.get("configActualSelectedQuestion"),
            $image = $form.find("#configEditQuestionsImage"),
            oldQuestion = Questions.findOne({_id: qId}),
            answerIsImage = oldQuestion.answerIsImage;

        if(answerIsImage){
            var goodAnswerImage = $goodA[0].files[0], 
                badAnswerImage1 = $badA1[0].files[0], 
                badAnswerImage2 = $badA2[0].files[0], 
                badAnswerImage3 = $badA3[0].files[0];

            if (goodAnswerImage) goodA = Images.insert(goodAnswerImage)._id; else goodA = oldQuestion.answers[0];
            if (badAnswerImage1) badA1 = Images.insert(badAnswerImage1)._id; else badA1 = oldQuestion.answers[1];
            if (badAnswerImage2) badA2 = Images.insert(badAnswerImage2)._id; else badA2 = oldQuestion.answers[2];
            if (badAnswerImage3) badA3 = Images.insert(badAnswerImage3)._id; else badA3 = oldQuestion.answers[3];
        }

        var file = $image[0].files[0];

        if (file) {
            Images.insert(file, function(err, fileObj) {
                if (!err) {
                    editQuestion(fileObj._id);
                };
            });
        } else {
            editQuestion("");
        }


        function editQuestion(image) {
            var post = {
                _id: qId,
                base: baseId,
                text: text,
                image: image,
                answers: [
                    goodA, badA1, badA2, badA3
                ]
            };
            Meteor.call('updateQuestionToKnowlegdeBase', post, function(error, result) {
                if (error) {
                    alert(error);
                } else {
                    $modal.modal('hide');
                }
            });
        }

    },
    "change #configAddQuestionsOptionsAnswerType": function(e) {
        var $chkbx = $(e.target);
        if($chkbx.prop('checked')){
            Session.set("configQuestionsAnswersType", "file");
        } else {
            Session.set("configQuestionsAnswersType", "text");
        }
    }
});