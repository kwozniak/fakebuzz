var hostAddr = "192.168.0.102";

Session.setDefault("players", []);

Session.setDefault("pageSelected", "1");

var perPage = 6;


function getQuestions(knowledgeBasesArray, amount) {
    var allQuestions = Questions.find({
        base: {
            $in: knowledgeBasesArray
        }
    }).fetch();
    var questionsArray = [];
    var i = 0,
        rand = 0;
    var item;
    for (i = 0; i < amount; i += 1) {
        rand = Math.floor(Math.random() * allQuestions.length);
        item = allQuestions[rand];
        questionsArray.push(item);
        allQuestions.splice(rand, 1);
    }

    return questionsArray;
}


Template.serverGameLobby.rendered = function() {
    Deps.autorun(function() {
        var baseId = Session.get("knowledgeSelectedId");
        Meteor.subscribe("questions", baseId);
    });

    var url = Router.url("clientJoinToGameWithCode", {
        code: (Session.get("gameData") || {}).code
    });
    url = url.replace("localhost", hostAddr);
    var qrcodesvg = new Qrcodesvg(url, "qrcode", 320, {
        "ecclevel": 1
    });
    qrcodesvg.draw({
        "method": "classic",
        "fill-colors": ["#000000", "#2D2D2D"],
        "fill-colors-scope": "square"
    }, {
        "stroke-width": 0
    });

    var socket = window.userSocketIO;
    socket.on('getAnswer', function(data) {

        var playerPoint = usersPoints.findOne({
            name: data.name
        });

        if (!playerPoint) {
            usersPoints.insert({
                points: 0,
                name: data.name,
                lastGamePoints: 0,
                lastAnswer: data.answer,
                connected: true,
                lastQuestion: Session.get("gameCurrentQuestion")
            });
        } else {
            usersPoints.update({
                name: data.name
            }, {
                $set: {
                    lastAnswer: data.answer,
                    lastQuestion: Session.get("gameCurrentQuestion")
                }
            });
        }

    });



    socket.on('userJoined', function(data) {
        var players = Session.get("players");
        players.push(data.name);
        Session.set("players", players);
    });

    socket.on('userDisconnected', function(data) {
        var players = Session.get("players");
        var i = players.indexOf(data.name);
        if (i > -1) {
            players.splice(i, 1);
        }
        Session.set("players", players);
        Meteor.call('setPlayerAsDisconnected', {
            name: data.name,
            code: Session.get("gameData").code
        }, function(error, result) {
            if (!error) {
                usersPoints.update({
                    name: data.name
                }, {
                    $set: {
                        connected: false
                    }
                });
            }
        });
    });

};


Template.serverGameLobby.helpers({
    "game": function() {
        return Session.get("gameData") || {};
    },
    player: function() {
        var players = Session.get("players");
        var game = Games.findOne({
            "players.name": {
                $in: players
            }
        });
        if (game)
            return game.players;
    },
    "getUrl": function() {
        var url = Router.url("clientJoinToGameWithCode", {
            code: (Session.get("gameData") || {}).code
        });
        url = url.replace("localhost", hostAddr);
        return url;
    }
});

Template.serverGameLobby.events({
    'click #newGame': function(e) {
        var _settings = {
            amountOfQuestions: 5,
            nextQuestionDelayTime: 3500,
            bases: ["4NnX8xHm5zTAdFSpJ"]
        }
        Session.set("gameSettings", _settings);
        var questions = getQuestions([Session.get("knowledgeSelectedId")], _settings.amountOfQuestions);
        Session.set("gameQuestions", questions);
        Session.set("gameCurrentQuestion", 0);
        Router.go("serverGame");
    }
});


Template.chooseGameCategory.helpers({
    categories: function() {
        return GameTypes.find();
    },
    isCategorySelected: function() {
        return Session.equals('categorySelected', this.name);
    }
});

Template.chooseGameCategory.events({
    // 'click .game-category': function() {
    //     if (Session.equals('categorySelected', this.name)) {
    //         Session.set('categorySelected', null);
    //     } else {
    //         Session.set('categorySelected', this.name);
    //     }
    // }
});

Template.chooseKnowledgeCategory.helpers({
    categories: function() {
        return KnowledgeBases.find({});
    },
    isCategorySelected: function() {
        return Session.equals('knowledgeSelected', this.name);
    },
    sites: function() {
        var pages = [];
        var elementsCount = KnowledgeBases.find().count();

        for (var i = 1; i <= (elementsCount % perPage > 0 ? (elementsCount / perPage) + 1 : elementsCount / perPage); i++) {
            pages.push(i);
        };
        return pages;
    },
    isPageSelected: function(val) {
        return Session.equals('pageSelected', val + "");
    },
    getImagUrl: function(id) {
        var image = Images.find(id).fetch()[0];
        if (image) {
            return image.url();
        }
    }
});

Template.chooseKnowledgeCategory.events({
    'click .game-category': function() {
        // if (Session.equals('knowledgeSelected', this.name)) {
        //     Session.set('knowledgeSelected', null);
        //     Session.set('knowledgeSelectedId', null);
        // } else {
        //     Session.set('knowledgeSelected', this.name);
        //     Session.set('knowledgeSelectedId', this._id);
        // }
    },
    'click .current-page': function() {
        // Session.set('pageSelected', this + "");
    }
});

Template.chooseKnowledgeCategory.rendered = function() {
    var $frame = $('#knowledgebase-picker');
    var $wrap = $frame.parent();

    // Call Sly on frame
    $frame.sly({
        horizontal: 1,
        itemNav: 'forceCentered',
        smart: 1,
        activateMiddle: 1,
        activateOn: 'click',
        mouseDragging: 1,
        touchDragging: 1,
        releaseSwing: 1,
        startAt: 0,
        scrollBar: $wrap.find('.scrollbar'),
        scrollBy: 1,
        speed: 300,
        elasticBounds: 1,
        easing: 'swing',
        dragHandle: 1,
        dynamicHandle: 1,
        clickBar: 1,

        // Buttons
        prev: $wrap.find('.prev'),
        next: $wrap.find('.next')
    });
    $frame.sly('on', 'active', function(a, b, c) {
        var e = $(this.items[b].el);
        Session.set('knowledgeSelected', e.attr("_name"));
        Session.set('knowledgeSelectedId', e.attr("_id"));
    });
    $frame.sly('on', 'load', function() {
        var e = $(this.items[0].el);
        Session.set('knowledgeSelected', e.attr("_name"));
        Session.set('knowledgeSelectedId', e.attr("_id"));
    });
};

Template.chooseGameCategory.rendered = function() {
    var $frame = $('#gametype-picker');
    var $wrap = $frame.parent();

    // Call Sly on frame
    $frame.sly({
        horizontal: 1,
        itemNav: 'forceCentered',
        smart: 1,
        activateMiddle: 1,
        activateOn: 'click',
        mouseDragging: 1,
        touchDragging: 1,
        releaseSwing: 1,
        startAt: 0,
        scrollBar: $wrap.find('.scrollbar'),
        scrollBy: 1,
        speed: 300,
        elasticBounds: 1,
        easing: 'swing',
        dragHandle: 1,
        dynamicHandle: 1,
        clickBar: 1,

        // Buttons
        prev: $wrap.find('.prev'),
        next: $wrap.find('.next')
    });
    $frame.sly('on', 'active', function(a, b, c) {
        var e = $(this.items[b].el);
        Session.set('categorySelected', e.attr("_name"));
    });
    $frame.sly('on', 'load', function() {
        var e = $(this.items[0].el);
        Session.set('categorySelected', e.attr("_name"));
    });
};