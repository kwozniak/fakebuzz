var shuffledAnswers = [];
var handler = null;

function getShuffledArray(a) {
    var o = a.slice(0);
    for (var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
};

function isGoodAnswer(answer) {
    var questions = Session.get("gameQuestions");
    var currentQuestion = Session.get("gameCurrentQuestion");
    var question = questions[currentQuestion];
    var answersArray = question.answers;
    var index;
    switch (answer) {
        case 'A':
            index = 0;
            break;
        case 'B':
            index = 1;
            break;
        case 'C':
            index = 2;
            break;
        case 'D':
            index = 3;
            break;
    }
    return shuffledAnswers[index] === answersArray[0];
}

function endOfQuiz() {
    usersPoints.find({}).forEach(function(user) {
        usersPoints.update({
            _id: user._id
        }, {
            $inc: {
                points: user.lastGamePoints
            },
            $set: {
                lastGamePoints: 0,
                lastAnswer: ""
            }
        });
    });
}

function changeQuestion(argument) {
    $("[name='indicator']").removeClass("glyphicon glyphicon-ok goodAnswer");
    $("[name='indicator']").removeClass("glyphicon glyphicon-remove badAnswer");
    var questions = Session.get("gameQuestions");
    // usersPoints.find({}).forEach(function(user) {
        usersPoints.update({
            // _id: user._id
        }, {
            // $inc: {

            // },
            $set: {
                lastAnswer: ""
            }
        }, {
            multi: true
        });
    // });

    var currentQuestion = Session.get("gameCurrentQuestion");
    $(".gameAnswer.goodAnswer").removeClass("goodAnswer");
    currentQuestion += 1;
    Session.set("gameCurrentQuestion", currentQuestion);
    if (currentQuestion == questions.length) {
        endOfQuiz();
    };
}

function nextQuestion() {
    var currentQuestion = Session.get("gameCurrentQuestion");
    var settings = Session.get("gameSettings");
    var questions = Session.get("gameQuestions");
    var question = questions[currentQuestion];
    var answersArray = question.answers;
    var $answer = $(".gameAnswer [text='" + answersArray[0] + "']");
    $answer.closest("div.gameAnswer").addClass("goodAnswer");

    usersPoints.find({}).forEach(function(user) {
        var _class = "glyphicon glyphicon-remove badAnswer";
        if (isGoodAnswer(user.lastAnswer)) {
            usersPoints.update({
                _id: user._id
            }, {
                $inc: {
                    lastGamePoints: 1
                }
            });
            _class = "glyphicon glyphicon-ok goodAnswer";
        };
        $("[playerName='" + user.name + "']").find("[name='indicator']").addClass(_class);
    });
    setTimeout(changeQuestion, settings.nextQuestionDelayTime);
}



function checkAnswers(answer) {
    var votedAmount = usersPoints.find({
        connected: true,
        lastAnswer: {
            $in: ['A', 'B', 'C', 'D']
        }
    }).count();
    if (votedAmount > 0) {
        var playersArray = Games.findOne().players;
        var filteredPlayersArray = [];
        _.each(playersArray, function(player) {
            if (player.connected) {
                filteredPlayersArray.push(player);
            };
        });
        if (votedAmount >= filteredPlayersArray.length) {
            if (answer && answer.lastAnswer && answer.lastAnswer != "") {

            } else {
                nextQuestion();
            }
        };
    };
}

Template.basicQuiz.rendered = function() {
    var query = usersPoints.find({
        connected: true,
        lastAnswer: {
            $in: ['A', 'B', 'C', 'D']
        }
    });

    if (typeof handler !== "undefined" && handler !== null) {
        if (typeof handler.stop === "function") {
            handler.stop();
        }
    }

    handler = query.observe({
        added: function(newDocument) {
            checkAnswers(null);
        },
        changed: function(newDocument, oldDocument) {
            checkAnswers(oldDocument);
        }
    });
};

Template.basicQuiz.helpers({
    answers: function() {
        var currentQuestion = Session.get("gameCurrentQuestion");
        var questions = Session.get("gameQuestions");
        if (questions) {
            var question = questions[currentQuestion];
            if (question) {
                shuffledAnswers = getShuffledArray(question.answers);
                var answers = {
                    A: shuffledAnswers[0],
                    B: shuffledAnswers[1],
                    C: shuffledAnswers[2],
                    D: shuffledAnswers[3]
                };
                return answers;
            } else {
                return null;
            }
        };
    },
    question: function() {
        var currentQuestion = Session.get("gameCurrentQuestion");
        var questions = Session.get("gameQuestions");
        if (questions && questions[currentQuestion]) {
            return questions[currentQuestion];
        };
    },
    knowledgBases: function() {
        return [];
    },
    player: function() {
        var players = Session.get("players");
        var game = Games.findOne({
            "players.name": {
                $in: players
            }
        });
        if (game)
            return game.players;
    },
    playerPoints: function(name) {
        return usersPoints.findOne({
            name: name
        });
    },
    getImageUrl: function(id) {
        var image = Images.find(id).fetch()[0];
        if (image) {
            return image.url();
        }
    }
});