function idIsValid(id) {
    return (_.isString(id) && id.length > 0);
}

function joinToGame(params, socket) {
    Meteor.call("joinToGame", params, function(err, ret) {
        if (!err) {
            socket.on("joinedToGame",function(data) {
                console.log(data);
                Session.set("playerData", data);
                Router.go("gameController", ret);
            });
            params.isServer = false;
            socket.emit("joinToGame", params ,function(joinErr,joinRet) {
                console.log(joinErr,joinRet);
            });
        } else {
            alert(err);
        }
    });
}

Template.clientJoinToGame.events({
    'submit [name="game-join-form"]': function(event) {
        event.preventDefault();
        var $form = $(event.target),
            $gameCodeInput = $form.find("#game-code-input"),
            gameCode = $gameCodeInput.val(),
            $nameInput = $form.find("#name-input"),
            playerName = $nameInput.val();
        if (idIsValid(gameCode)) {
            var params = {};
            params.code = gameCode;
            params.name = playerName;

            if (!window.userSocketIO) {
                var socket = io(window.location.hostname + ":80");
                socket.on('connect', function() {
                    window.userSocketIO = socket;
                    joinToGame(params, socket);
                });
                socket.on('disconnect', function() {
                    window.userSocketIO = null;
                    Router.go("disconnected");
                });
            } else {
                joinToGame(params, window.userSocketIO);
            }
        } else {
            alert("Podaj ID");
        }

    }

});


Template.clientJoinToGame.helpers({
    "code": function() {
        return Router.getData();
    }
});