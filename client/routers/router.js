layoutController = RouteController.extend({
    layoutTemplate: 'layout',
    yieldTemplates: {
        'header': {
            to: 'header'
        },
        'footer': {
            to: 'footer'
        }
    }
});



Router.map(function() {
    this.route('homePage', {
        path: '/',
        controller: layoutController,
        waitOn: [
            function() {
                return this.subscribe("gameTypes")
            },
            function() {
                return this.subscribe("knowledgeBases");
            }
        ]
    });

    this.route('serverGameLobby', {
        path: '/server/lobby',
        controller: layoutController,
        waitOn: [
            function() {
                return this.subscribe("games", Session.get("gameData"))
            },
            function() {
                return this.subscribe("gameTypes")
            },
            function() {
                return this.subscribe("knowledgeBases");
            }
        ]
    });

    this.route('serverGame', {
        path: '/server/game',
        controller: layoutController,
        waitOn: [
            function() {
                return this.subscribe("games", Session.get("gameData"))
            },
            function() {
                return this.subscribe("gameTypes")
            },
            function() {
                return this.subscribe("knowledgeBases");
            }
        ]
    });

    this.route('clientJoinToGame', {
        path: '/client/join',
        controller: layoutController
    });

    this.route('clientJoinToGameWithCode', {
        template: "clientJoinToGame",
        path: '/client/join-with-code=:code',
        controller: layoutController,
        data: function() {
            return this.params.code;
        }
    });

    this.route('gameController', {
        path: '/client/play',
        controller: layoutController
    });

    this.route('disconnected', {
        path: '/disconnected',
        controller: layoutController
    });

    this.route('config', {
        path: '/config',
        controller: layoutController,
        waitOn: [
            function() {
                return this.subscribe("knowledgeBases");
            }
        ]
    });

});