Games = new Meteor.Collection("games");
GameTypes = new Meteor.Collection("gameTypes");
KnowledgeBases = new Meteor.Collection("knowledgeBases");
Questions = new Meteor.Collection("questions");

Images = new FS.Collection("images", {
    useHTTP: true,
    stores: [new FS.Store.FileSystem("images", {
        path: "~/images"
    })],
    filter: {
        maxSize: 1048576, //in bytes
        allow: {
            contentTypes: ['image/*']
        },
        onInvalid: function(message) {
            if (Meteor.isClient) {
                alert(message);
            } else {
                console.log(message);
            }
        }
    }
});

Images.allow({
    insert: function(userId, doc) {
        if (userId) {
            return true;
        } else {
            return false;
        }
    },
    update: function(userId, doc, fields, modifier) {
        if (userId) {
            return true;
        } else {
            return false;
        }
    },
    download: function() {
        return true;
    }

});